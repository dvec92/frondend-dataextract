import { Component } from '@angular/core';
import {FileUploadService} from "./service/file-upload.service";
import {FileUploadModule} from "primeng/fileupload";
import {ResponseData} from "./models/ResponseData";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FrontEnd';
  file: any;
  data: any;
  response: ResponseData | undefined;
  ingresosVsGastos: any;
  ingresosAnuales: any;
  multiAxisOptions: any;

  constructor(private fileUploadService: FileUploadService) {
  }

  uploadXML(event: any){
    console.log("SE paso");
    for (let file of event.files){
      this.file = file;
    }
    this.uploadFile();
  }


  private uploadFile() {
    this.fileUploadService.postFile(this.file).subscribe(
      data => {
        console.log(data);
        this.llenarIngresosVsGastos(data);
        this.llenarIngresosAnuales(data);
      });
  }

  llenarIngresosVsGastos(data: any){
      this.ingresosVsGastos = {
        labels: data.aniosFlujoDeCaja,
        datasets: [
          {
            label: 'Ingresos',
            backgroundColor: "#6ebe71",
            data: data.totalIngresos
          },
          {
            label: 'Gastos',
            backgroundColor: "#2196f3",
            data: data.totalGastos
          }
        ]
      }
  }

  llenarIngresosAnuales(data: any) {
    this.ingresosAnuales = {
      labels: data.aniosFlujoDeCaja,
      datasets:[
        {
          label: 'Ingresos por alquileres',
          data: data.ingresos['Ingresos por alquileres'],
          fill: false,
          borderColor: '#42A5F5',
          tension: .4
        },
        {
          label: 'Ingresos por ventas de productos/servicios',
          data: data.ingresos['Ingresos por ventas de productos/servicios'] ,
          fill: false,
          borderColor: '#42A5F5',
          tension: .4
        }
      ]
    }
  }
}
