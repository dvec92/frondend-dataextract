import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from '@angular/common/http';
import {ResponseData} from "../models/ResponseData";

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  PATH_SERVER = 'http://ec2-54-175-45-98.compute-1.amazonaws.com:5000/api/1.0/extractData/load';

  constructor(private httpClient: HttpClient) { }

  postFile(file: File): Observable<any> {
      let testData:FormData = new FormData();
      testData.append('file', file, file.name);
      return this.httpClient.post(this.PATH_SERVER, testData);
  }
}
