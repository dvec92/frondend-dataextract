import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FileUploadModule } from "primeng/fileupload";
import { ArchivoComponent } from './archivo/archivo.component';
import {HttpClientModule} from "@angular/common/http";
import {MessageService} from "primeng/api";
import {ChartModule} from "primeng/chart";

@NgModule({
  declarations: [
    AppComponent,
    ArchivoComponent
  ],
  imports: [
    BrowserModule,
    FileUploadModule,
    HttpClientModule,
    ChartModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
