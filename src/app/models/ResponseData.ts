import {BigInteger} from "@angular/compiler/src/i18n/big_integer";

export interface ResponseData{
  titulo: String;
  periodos: String;
  moneda: String;
  inversion: Map<String, BigInteger>;
  aniosFlujoDeCaja: String[];
  ventas: Map<String, BigInteger[]>;
  ingresos: Map<String, BigInteger[]>;
  totalIngresos: BigInteger[]
  gastos: Map<String, BigInteger[]>;
  otrosRubros: Map<String, BigInteger[]>;
  totalGastos: BigInteger[];
  flujoCajaAntesImpuestos: Map<String, BigInteger[]>;
  impuestoSobreRenta: Map<String, BigInteger[]>;

}
