import { Component, OnInit } from '@angular/core';
import {FileUploadService} from "../service/file-upload.service";

@Component({
  selector: 'app-archivo',
  templateUrl: './archivo.component.html',
  styleUrls: ['./archivo.component.css']
})
export class ArchivoComponent implements OnInit {
  uploadedFiles: any[] = [];
  constructor(private fileUploadService: FileUploadService) { }

  ngOnInit(): void {
  }

}
